import numpy as np
from numpy.core.defchararray import array
import matplotlib.pyplot as plt

import math
import time

# Model Parameter
W = 0.4 # Distance between 2 wheel
H = 0.01 # Distance between Control point and Center point
r = 0.07 # Wheel radius
max_vel = 10 # rad/s

dt = 0.001 # Time step
T = 30 # Time length
n = (int)(T/dt) # Number of loop

# Controller 1
K = 10*np.eye(2) # P gain matrix
# Controller 2
k_rho = 3
k_alpha = 10
k_beta = 10

sign = 1

def main():
    q = np.zeros((n,3),dtype=float)
    q[0,2]=-3*math.pi/4 # Robot position q[0] = x, q[1] = y, q[2] = theta
    q[0,0] = 5
    q[0,1] = 5

    # Robot trajectory
    qd = np.zeros((n,3),dtype=float)
    dqd = np.zeros((n,3), dtype=float)
    for i in range(n):
        qd[i,2] = -math.pi/2

    # # Create circle trajectory
    # psi = 0 # angle
    # radius = 2.5 # radius
    # for i in range(n):
    #     qd[i,0] = radius*math.cos(psi) # x pos
    #     qd[i,1] = radius*math.sin(psi) # y pos
    #     qd[i,2] = psi + math.pi/2 # angle pos
    #     psi += math.pi*2/n
    # for i in range(n-1):
    #     dqd[i,0] = (qd[i+1,0]-qd[i,0])/dt
    #     dqd[i,1] = (qd[i+1,1]-qd[i,1])/dt
    #     dqd[i,2] = (qd[i+1,2]-qd[i,2])/dt

    for i in range(n-1):
        # Calculate control signal
        #u = Controller(q[i], qd[i], dqd[i])
        u = Controller2(q[i], qd[i], dqd[i])
        # Robot model
        q[i+1] = Simulation(q[i], u)
        
    # Plot result
    for i in range((int)(n/100)-1):
        plt.clf()
        PlotAnimation(q[100*i], qd[100*i])
        plt.xlim([-5.0, 5.0])
        plt.title("Running process ...")
        plt.pause(dt)
        
    # plt.pause(5)
    plt.close()
    # Plot pose error
    t = np.linspace(0,T,n)
    plt.plot(t,q[:,0]-qd[:,0],label="x_error")
    plt.plot(t,q[:,1]-qd[:,1],label="y_error")
    plt.plot(t,q[:,2]-qd[:,2],label="theta_error")
    plt.xlabel("time(s)")
    plt.ylabel("position error (m)")
    plt.legend()
    plt.pause(10)
    wait = input()
    plt.close()
    # Plot pose error
    t = np.linspace(0,T,n)
    plt.plot(q[:,0],q[:,1],label="real pose")
    plt.plot(qd[:,0],qd[:,1],label="desired pose")
    plt.xlabel("time(s)")
    plt.ylabel("position error (m)")
    plt.legend()
    plt.pause(10)
    wait = input()


def Controller(q, qd, dqd):
    # Point C to Point M
    # M real pose
    qM = np.array([
        q[0]+H*math.cos(q[2]),
        q[1]+H*math.sin(q[2]),
        q[2]
        ])
    # M desired pose
    qdM = np.array([
        qd[0]+H*math.cos(qd[2]),
        qd[1]+H*math.sin(qd[2]),
        qd[2]
        ])
    # M desired velocity
    dqdM = np.array([
        dqd[0]-H*dqd[2]*math.sin(qd[2]),
        dqd[1]+H*dqd[2]*math.cos(qd[2]),
        dqd[2]
    ])
    theta = q[2]
    # Error M
    e = np.array([
        qdM[0]-qM[0],
        qdM[1]-qM[1]
    ])
    # Translation velocity control
    vel_control = K@e + np.array([dqdM[0],dqdM[1]])
    P = np.array([
        [(W*math.sin(theta)+2*H*math.cos(theta))/(2*H*r),(-W*math.cos(theta)+2*H*math.sin(theta))/(2*H*r)],
        [(-W*math.sin(theta)+2*H*math.cos(theta))/(2*H*r),(W*math.cos(theta)+2*H*math.sin(theta))/(2*H*r)]
    ])
    # Wheel rotate velocity control
    dphi = P@vel_control
    dphi[0],dphi[1] = Rescale(dphi[0],dphi[1], max_vel)
    return dphi

def Controller2(q,qd,dqd):
    dx = qd[0] - q[0]
    dy = qd[1] - q[1]
    theta = q[2]
    # Distance to goal pose
    rho = math.sqrt(dx*dx + dy*dy)
    alpha = math.atan(dy/dx) - theta
    beta = qd[2] - theta
    alpha = AngleError(alpha)
    beta = AngleError(beta)
    global sign
    if (dx*math.cos(theta) + dy*math.sin(theta) < -0.1):
        sign = -1
    if (dx*math.cos(theta) + dy*math.sin(theta) > -0.1):
        sign = 1
    # Translation velocity
    vx = math.pow(math.pi/2 - math.fabs(alpha),6)*(k_rho*rho + math.sqrt(dqd[0]*dqd[0]+dqd[1]*dqd[1]))
    omega = k_alpha*alpha*math.tanh(rho) + k_beta*beta/(math.pow(10*rho+1,4)) + dqd[2]
    vx *= sign

    dphi_l = (1/r)*(vx - omega*W/2)
    dphi_r = (1/r)*(vx + omega*W/2)
    print(rho)
    dphi_l, dphi_r = Rescale(dphi_l, dphi_r, max_vel)

    return [dphi_l, dphi_r]

def Simulation(q, u):
    theta=q[2]
    delta_q = np.array([
        (r*math.cos(theta)/2)*(u[0]+u[1])*dt,
        (r*math.sin(theta)/2)*(u[0]+u[1])*dt,
        -(r/W)*(u[0]-u[1])*dt
    ])
    return delta_q + q

def flatten(a):
    return np.array(a).flatten()

def PlotAnimation(q,qd):
    DrawRobot(q, 'r')
    DrawRobot(qd, 'b')
    plt.axis("equal")
    plt.plot(-5,-5,"o")
    plt.plot(5,5,"o")

def DrawRobot(q, color): # 1 is red (for real state), 2 is blue (for desired state)
    # Robot dimension
    Width = 0.4
    Length = 0.6
    Wheel_width = 0.05
    theta = q[2]
    # Draw frame
    x_frame = np.array([
        q[0] + (Length/2)*math.cos(theta) - (Width/2)*math.sin(theta),
        q[0] - (Length/2)*math.cos(theta) - (Width/2)*math.sin(theta),
        q[0] - (Length/2)*math.cos(theta) + (Width/2)*math.sin(theta),
        q[0] + (Length/2)*math.cos(theta) + (Width/2)*math.sin(theta),
        q[0] + (Length/2)*math.cos(theta) - (Width/2)*math.sin(theta)
    ])
    y_frame = np.array([
        q[1] + (Length/2)*math.sin(theta) + (Width/2)*math.cos(theta),
        q[1] - (Length/2)*math.sin(theta) + (Width/2)*math.cos(theta),
        q[1] - (Length/2)*math.sin(theta) - (Width/2)*math.cos(theta),
        q[1] + (Length/2)*math.sin(theta) - (Width/2)*math.cos(theta),
        q[1] + (Length/2)*math.sin(theta) + (Width/2)*math.cos(theta)
    ])
    plt.plot(flatten(x_frame), flatten(y_frame), "-"+color)

def Rescale(x, y, max):
    scale = y/x
    if (math.fabs(x) >= math.fabs(y)):
        if math.fabs(x)<=max:
            return x,y
        elif x > max:
            x = max
            y = scale*x
        elif x < -max:
            x = -max
            y = scale*x
    if (math.fabs(x) < math.fabs(y)):
        if math.fabs(y)<=max:
            return x,y
        elif y > max:
            y = max
            x = y/scale
        elif y < -max:
            y = -max
            x = y/scale
    return x,y
            
def AngleError(delta_theta): # Fix angle to -pi/2 --> pi/2
    # Find region
    n = math.floor((delta_theta + math.pi/2)/(math.pi))
    delta = delta_theta - math.pi*n
    return delta

if __name__ == '__main__':
    main()