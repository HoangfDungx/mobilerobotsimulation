import numpy as np
from numpy.core.defchararray import array
import matplotlib.pyplot as plt

import math

# Model Parameter
Wb = Wf = 0.4 # Distance between 2 wheel
L = 0.4
rb = 0.07 # Wheel radius
rl = 0.05
max_vel = 10 # rad/s
max_vel_gamma = 1 # rad/s
max_gamma = 0.7854 # Max steering angle

# Check front
r = 0.5
dr = 0.05
sign_vel = 1

dt = 0.001 # Time step
T = 30 # Time length
n = (int)(T/dt) # Number of loop

# Controller gain
k_rho = 5
k_alpha = 10
k_beta = -5
k_gamma = 5

def main():
    q = np.zeros((n,3),dtype=float)
    q[0,0]= 0.2
    q[0,1]= 0.2
    q[0,2]= 0 # Robot position q[0] = x, q[1] = y, q[2] = theta
    gamma = np.zeros((n,2),dtype=float) # Steering angle gammal,gammar
    # Robot trajectory
    qd = np.zeros((n,3),dtype=float)
    dqd = np.zeros((n,3), dtype=float)

    # Create circle trajectory
    psi = 0 # angle
    radius = 2.5 # radius
    for i in range(n):
        qd[i,0] = radius*math.cos(psi) # x pos
        qd[i,1] = radius*math.sin(psi) # y pos
        qd[i,2] = psi + math.pi/2 # angle pos
        psi += math.pi*2/n
    for i in range(n-1):
        dqd[i,0] = (qd[i+1,0]-qd[i,0])/dt
        dqd[i,1] = (qd[i+1,1]-qd[i,1])/dt
        dqd[i,2] = (qd[i+1,2]-qd[i,2])/dt

    for i in range(n-1):
        # Calculate control signal
        u = Controller(q[i], qd[i], dqd[i], gamma[i])
        if i%100 == 0:
            print(sign_vel)
        # Robot model
        q[i+1], gamma[i+1] = Simulation(q[i], gamma[i], u)
        
    # Plot result
    for i in range((int)(n/100)-1):
        plt.clf()
        PlotAnimation(q[100*i], qd[100*i], gamma[100*i])
        plt.xlim([-5.0, 5.0])
        plt.pause(dt)
        
    # plt.pause(5)
    plt.close()
    # Plot pose error
    t = np.linspace(0,T,n)
    plt.plot(t,q[:,0]-qd[:,0],label="x_error")
    plt.plot(t,q[:,1]-qd[:,1],label="y_error")
    plt.plot(t,q[:,2]-qd[:,2],label="theta_error")
    plt.plot(t,gamma[:,0],label="gamma_l")
    plt.plot(t,gamma[:,1],label="gamma_r")
    plt.xlabel("time(s)")
    plt.ylabel("position error (m)")
    plt.legend()
    plt.pause(10)
    wait = input()
    plt.close()
    # Plot pose error
    t = np.linspace(0,T,n)
    plt.plot(q[:,0],q[:,1],label="real pose")
    plt.plot(qd[:,0],qd[:,1],label="desired pose")
    plt.xlabel("time(s)")
    plt.ylabel("position error (m)")
    plt.legend()
    plt.pause(10)
    wait = input()

def Controller(q,qd,dqd,gamma):
    dx = qd[0] - q[0]
    dy = qd[1] - q[1]
    theta = q[2]
    # Distance to goal pose
    rho = math.sqrt(dx*dx + dy*dy)
    alpha = math.atan2(dy,dx) - theta
    beta = qd[2] - theta - alpha
    #Redefine angle error to (-pi - pi)
    alpha = AngleError(alpha)
    beta = AngleError(beta)

    # Check front
    global sign_vel
    check_front = CheckFront(dx,dy,q[2])
    if (check_front > math.pow(r+dr,2)):
        sign_vel = 1
    if (check_front < math.pow(r-dr,2)):
        sign_vel = -1

    # Translation and rotation velocity
    omega = k_alpha*alpha + k_beta*beta + dqd[2]
    vx = k_rho*rho + math.sqrt(dqd[0]*dqd[0]+dqd[1]*dqd[1])
    vx *= sign_vel
    # omega *= sign_vel
    # Steering angle controller
    gammad = np.array([
        math.atan2(L*omega,vx)*sign_vel,
        math.atan2(L*omega,vx)*sign_vel
    ])
    gammad[0],gammad[1] = Rescale(gammad[0],gammad[1], max_gamma)
    e_gamma = gammad - gamma
    if (math.fabs(e_gamma[0]) > 0.3 or math.fabs(e_gamma[1] > 0.3)):
        vx=0
    dgamma = np.array([k_gamma*(gammad[0] - gamma[0]),k_gamma*(gammad[1] - gamma[1])])
    dgamma[0] = limit(dgamma[0], max_vel_gamma)
    dgamma[1] = limit(dgamma[1], max_vel_gamma)
    [dphi_l,dphi_r] = [(2*vx - Wb*omega)/(2*rb), (2*vx + Wb*omega)/(2*rb)]
    dphi_l, dphi_r = Rescale(dphi_l, dphi_r, max_vel)
    return [dphi_l, dphi_r, dgamma[0], dgamma[1]]

def cotg(gamma):
    if gamma == 0:
        return 1000000.0
    return 1/math.tan(gamma)

def Simulation(q, gamma, u):
    theta=q[2]
    gamma2 = gamma + np.array([dt*u[2],dt*u[3]])
    delta_q = np.array([
        math.cos(theta)*rb*(u[0] + u[1])*dt/2,
        math.sin(theta)*rb*(u[0] + u[1])*dt/2,
        rb*(u[0]+u[1])*dt/(L*(cotg(gamma[0])+cotg(gamma[1])))
    ])
    return delta_q + q, gamma2

def flatten(a):
    return np.array(a).flatten()

def PlotAnimation(q,qd, gamma):
    DrawRobot(q,gamma, 'r')
    DrawRobot(qd,np.array([0,0]), 'b')
    plt.axis("equal")
    plt.plot(-5,-5,"o")
    plt.plot(5,5,"o")

def DrawRobot(q,gamma, color): # 1 is red (for real state), 2 is blue (for desired state)
    # Robot dimension
    Width = 0.4
    Length = 0.6
    l = 0.1
    Wheel_width = 0.05
    theta = q[2]
    # Draw frame
    x_frame = np.array([
        q[0] + (Length/2)*math.cos(theta) - (Width/2)*math.sin(theta),
        q[0] - (Length/2)*math.cos(theta) - (Width/2)*math.sin(theta),
        q[0] - (Length/2)*math.cos(theta) + (Width/2)*math.sin(theta),
        q[0] + (Length/2)*math.cos(theta) + (Width/2)*math.sin(theta),
        q[0] + (Length/2)*math.cos(theta) - (Width/2)*math.sin(theta)
    ])
    y_frame = np.array([
        q[1] + (Length/2)*math.sin(theta) + (Width/2)*math.cos(theta),
        q[1] - (Length/2)*math.sin(theta) + (Width/2)*math.cos(theta),
        q[1] - (Length/2)*math.sin(theta) - (Width/2)*math.cos(theta),
        q[1] + (Length/2)*math.sin(theta) - (Width/2)*math.cos(theta),
        q[1] + (Length/2)*math.sin(theta) + (Width/2)*math.cos(theta)
    ])
    # Draw steering wheel
    x_steer_l = np.array([
        q[0] + (Length/2)*math.cos(theta) - (Width/2)*math.sin(theta),
        q[0] + (Length/2)*math.cos(theta) - (Width/2)*math.sin(theta) + l*math.cos(theta+gamma[0])
    ])
    y_steer_l = np.array([
        q[1] + (Length/2)*math.sin(theta) + (Width/2)*math.cos(theta),
        q[1] + (Length/2)*math.sin(theta) + (Width/2)*math.cos(theta) + l*math.sin(theta+gamma[0])
    ])
    x_steer_r = np.array([
        q[0] + (Length/2)*math.cos(theta) + (Width/2)*math.sin(theta),
        q[0] + (Length/2)*math.cos(theta) + (Width/2)*math.sin(theta) + l*math.cos(theta+gamma[1])
    ])
    y_steer_r = np.array([
        q[1] + (Length/2)*math.sin(theta) - (Width/2)*math.cos(theta),
        q[1] + (Length/2)*math.sin(theta) - (Width/2)*math.cos(theta) + l*math.sin(theta+gamma[1])
    ])
    plt.plot(flatten(x_frame), flatten(y_frame), "-"+color)
    plt.plot(flatten(x_steer_l), flatten(y_steer_l), "-"+color)
    plt.plot(flatten(x_steer_r), flatten(y_steer_r), "-"+color)

def Rescale(x, y, max):
    scale = y/x
    if (math.fabs(x) >= math.fabs(y)):
        if math.fabs(x)<=max:
            return x,y
        elif x > max:
            x = max
            y = scale*x
        elif x < -max:
            x = -max
            y = scale*x
    if (math.fabs(x) < math.fabs(y)):
        if math.fabs(y)<=max:
            return x,y
        elif y > max:
            y = max
            x = y/scale
        elif y < -max:
            y = -max
            x = y/scale
    return x,y

def limit(x,max):
    if x > max:
        return max
    if x < -max:
        return -max
    return x

def AngleError(delta_theta): # Fix angle error to (-pi - pi)
    # Find region
    n = math.floor((delta_theta + math.pi)/(math.pi*2))
    delta = delta_theta - math.pi*n*2
    return delta

def CheckFront(dx,dy, theta):
    # Transform to robot coordinate frame
    x = dx*math.cos(theta) + dy*math.sin(theta)
    y = -dx*math.sin(theta) + dy*math.cos(theta)

    a = (x-r)*(x-r)+y*y
    b = (x+r)*(x+r)+y*y

    if (a<b):
        return a
    else:
        return b

if __name__ == '__main__':
    main()