import numpy as np
from numpy.core.defchararray import array
import matplotlib.pyplot as plt

import math
import time

# Model Parameter
W = 0.6 # Rectangle side
r = 0.07 # Wheel radius
max_vel = 10 # rad/s
j = math.sqrt(2)/2

dt = 0.001 # Time step
T = 40 # Time length
n = (int)(T/dt) # Number of loop

# Controller 1
K = np.array([
    [10, 0, 0],
    [0, 10, 0],
    [0, 0, 25]
]) # P gain matrix

def main():
    q = np.zeros((n,3),dtype=float)
    q[0,2]=math.pi/2 # Robot position q[0] = x, q[1] = y, q[2] = theta

    # Robot trajectory
    qd = np.zeros((n,3),dtype=float)
    dqd = np.zeros((n,3), dtype=float)

    # Create circle trajectory
    psi = 0 # angle
    radius = 2.5 # radius
    for i in range(n):
        qd[i,0] = radius*math.cos(psi) # x pos
        qd[i,1] = radius*math.sin(psi) # y pos
        qd[i,2] = psi + math.pi/2 + i*math.pi/(2*n) # angle pos
        psi += math.pi*2/n
    for i in range(n-1):
        dqd[i,0] = (qd[i+1,0]-qd[i,0])/dt
        dqd[i,1] = (qd[i+1,1]-qd[i,1])/dt
        dqd[i,2] = (qd[i+1,2]-qd[i,2])/dt

    for i in range(n-1):
        # Calculate control signal
        u = Controller(q[i], qd[i], dqd[i])
        # Robot model
        q[i+1] = Simulation(q[i], u)
        
    # Plot result
    for i in range((int)(n/100)-1):
        plt.clf()
        PlotAnimation(q[100*i], qd[100*i])
        plt.xlim([-5.0, 5.0])
        plt.title("Running process ...")
        plt.pause(dt)
        
    # plt.pause(5)
    plt.close()
    # Plot pose error
    t = np.linspace(0,T,n)
    plt.plot(t,q[:,0]-qd[:,0],label="x_error")
    plt.plot(t,q[:,1]-qd[:,1],label="y_error")
    plt.plot(t,q[:,2]-qd[:,2],label="theta_error")
    plt.xlabel("time(s)")
    plt.ylabel("position error (m)")
    plt.legend()
    plt.pause(10)
    wait = input()
    plt.close()
    # Plot pose error
    t = np.linspace(0,T,n)
    plt.plot(q[:,0],q[:,1],label="real pose")
    plt.plot(qd[:,0],qd[:,1],label="desired pose")
    plt.xlabel("time(s)")
    plt.ylabel("position error (m)")
    plt.legend()
    plt.pause(10)
    wait = input()


def Controller(q, qd, dqd):
    theta = q[2]
    # Error M
    e = np.array([
        qd[0]-q[0],
        qd[1]-q[1],
        qd[2]-q[2]
    ])
    # Translation velocity control
    vel_control = K@e + np.array([dqd[0],dqd[1],dqd[2]])
    vx = vel_control[0]*math.cos(theta)+vel_control[1]*math.sin(theta)
    vy = -vel_control[0]*math.sin(theta)+vel_control[1]*math.cos(theta)
    dtheta = vel_control[2]
    # Wheel rotate velocity control
    dphi = np.array([
        (1/r)*(-j*vx+j*vy+j*W*dtheta),
        (1/r)*(-j*vx-j*vy+j*W*dtheta),
        (1/r)*(j*vx-j*vy+j*W*dtheta),
        (1/r)*(j*vx+j*vy+j*W*dtheta)
    ])
    dphi = Rescale(dphi, max_vel)
    return dphi

def Simulation(q, u):
    theta=q[2]
    A = (-u[0] - u[1] + u[2] + u[3])*dt
    B = (u[0] - u[1] - u[2] + u[3])*dt
    C = (u[0] + u[1] + u[2] + u[3])*dt
    delta_q = np.array([
        (j/2)*r*math.cos(theta)*A - (j/2)*r*math.sin(theta)*B,
        (j/2)*r*math.sin(theta)*A + (j/2)*r*math.cos(theta)*B,
        (j/2)*(r/W)*C
    ])
    return delta_q + q

def flatten(a):
    return np.array(a).flatten()

def PlotAnimation(q,qd):
    DrawRobot(q, 'r')
    DrawRobot(qd, 'b')
    plt.axis("equal")
    plt.plot(-5,-5,"o")
    plt.plot(5,5,"o")

def DrawRobot(q, color): # 1 is red (for real state), 2 is blue (for desired state)
    # Robot dimension
    Width = Length = 0.6
    theta = q[2]
    # Draw frame
    x_frame = np.array([
        q[0] + (Length/2)*math.cos(theta) - (Width/2)*math.sin(theta),
        q[0] - (Length/2)*math.cos(theta) - (Width/2)*math.sin(theta),
        q[0] - (Length/2)*math.cos(theta) + (Width/2)*math.sin(theta),
        q[0] + (Length/2)*math.cos(theta) + (Width/2)*math.sin(theta),
        q[0] + (Length/2)*math.cos(theta) - (Width/2)*math.sin(theta)
    ])
    y_frame = np.array([
        q[1] + (Length/2)*math.sin(theta) + (Width/2)*math.cos(theta),
        q[1] - (Length/2)*math.sin(theta) + (Width/2)*math.cos(theta),
        q[1] - (Length/2)*math.sin(theta) - (Width/2)*math.cos(theta),
        q[1] + (Length/2)*math.sin(theta) - (Width/2)*math.cos(theta),
        q[1] + (Length/2)*math.sin(theta) + (Width/2)*math.cos(theta)
    ])
    plt.plot(flatten(x_frame), flatten(y_frame), "-"+color)

def Rescale(x, max):
    # Find max
    max_index = 0
    for i in range(1,3):
        if math.fabs(x[i]) > math.fabs(x[max_index]):
            max_index = i
    # Calc scale
    scale = np.ones(3, dtype=float)
    for i in range(3):
        scale[i] = x[i]/x[max_index]
    # Rescale
    if (x[max_index] > max):
        x[max_index] = max
    elif (x[max_index] < -max):
        x[max_index] = -max
    for i in range(3):
        x[i] = scale[i]*x[max_index]
    return x
            
if __name__ == '__main__':
    main()